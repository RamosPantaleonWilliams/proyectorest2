<?php

$link = new mysqli();

function conect() {
    $db = "api";
    $host = "localhost";
    $user = "root";
    $passwd = "";
    $GLOBALS['link'] = new mysqli($host, $user, $passwd, $db) or die("Error " . mysqli_error($link));
}

function getArray($result) {
    $arre = Array();
    $i = 0;
    $rows = mysqli_num_rows($result);
    while ($rows > $i) {
        $arre[$i++] = mysqli_fetch_assoc($result);
        //($result)
    }
    return $arre;
}

function getObject($result) {
    return mysqli_fetch_object($result);
}

function camposTabla($tabla) {
    conect();
    $sts = mysqli_query($GLOBALS['link'], "select * from $tabla limit 1");
    $array = $sts->fetch_fields();
    $s = array();
    foreach ($array as $key => $value) {
        $s[$key] = $value->name;
    }
    return $s;
}

function getEntidades($fields = "*", $where = "", $order_by = "", $limit = "") {
    $sql = "select $fields from entidad $where $order_by $limit ;";
    $sts = $GLOBALS['link']->query($sql);
    $arre = getArray($sts);
    return $arre;
}

function getEntidad($id, $fields = "*") {
    $sql = "select $fields from entidad where clave_entidad=$id";
    $sts = $GLOBALS['link']->query($sql);
    return getObject($sts);
}

function getMunicipios($id, $fields = "*", $where = "", $order_by = "", $limit = "") {
    if ($where == "")
        $where = "where ";
    else
        $where = "$where and ";
    $sql = "select $fields from entidad inner join municipio using(clave_entidad) $where clave_entidad=$id $order_by $limit";
    $sts = $GLOBALS['link']->query($sql);
    return getArray($sts);
}

function getMunicipio($id, $mun, $fields = "*") {

    $sql = "select $fields from municipio where clave_entidad=$id and clave_municipio=$mun";
    $sts = $GLOBALS['link']->query($sql);
    return getObject($sts);
}

function getIndicadores($id, $mun, $fields = "*", $where = "", $order_by = "", $limit = "") {
    if ($where == "")
        $where = "where ";
    else
        $where = "$where and ";
    $sql = "select $fields from indicador $where clave_entidad=$id and clave_municipio=$mun $order_by $limit";
    $sts = $GLOBALS['link']->query($sql);

    return getArray($sts);
}

function getInds($id_tema3, $fields = "*", $where = "", $order_by = "", $limit = "") {
    if ($where == "")
        $where = "where ";
    else
        $where = "$where and ";

    $sql = "select  $fields from indicador $where id_tema3=$id_tema3 $order_by $limit";
    $sts = $GLOBALS['link']->query($sql);
    return getArray($sts);
}

function getInd($id_tema3, $id, $anio = 0, $fields = "*", $where = "", $order_by = "", $limit = "") {
    if ($where == "")
        $where = "where ";
    else
        $where = "$where and ";
    if ($anio == 0) {

        $sql = "select $fields from indicador inner join valor using(id_indicador) $where id_tema3=$id_tema3 and id_indicador=$id $order_by $limit";
        $sts = $GLOBALS['link']->query($sql);
        return getArray($sts);
    } else {

        $sql = "select $fields from indicador inner join valor using(id_indicador) where id_tema3=$id_tema3 and id_indicador=$id and anio=$anio";
        $sts = $GLOBALS['link']->query($sql);
        return getObject($sts);
    }
}

function unique($tabla, $campo, $valor) {
    conect();
    $sql = "select * from $tabla where $campo='$valor'";
    $sts = $GLOBALS['link']->query($sql);
    $result = getObject($sts);
    if ($result) {
        $GLOBALS['response']->header("accept","Ese valor ya se encuentra registrado");
        return false;
    }
    else
        return true;
}

function getIndicador($id, $mun, $ind, $anio = -1, $fields = "*", $where = "", $order_by = "", $limit = "") {
    if ($where == "")
        $where = "where ";
    else
        $where = "$where and ";
    if ($anio == -1) {
        $sql = "select $fields from indicador left join valor using(id_indicador) $where clave_entidad=$id and clave_municipio=$mun and id_indicador=$ind $order_by $limit";
        $sts = $GLOBALS['link']->query($sql);
        return getArray($sts);
    } else {

        $sql = "select $fields from indicador inner join valor using(id_indicador) where clave_entidad=$id and clave_municipio=$mun and id_indicador=$ind and anio=$anio";
        $sts = $GLOBALS['link']->query($sql);
        return getObject($sts);
    }
}

function getTemasNivel1($fields = "*", $where = "", $order_by = "", $limit = "") {
    $sql = "select $fields from tema1 $where $order_by $limit";
    $sts = $GLOBALS['link']->query($sql);
    return getArray($sts);
}

function getTemaNivel1($id, $fields = "*") {

    $sql = "select $fields from tema1 where id_tema1=$id";
    $sts = $GLOBALS['link']->query($sql);
    return getObject($sts);
}

function getTemasNivel2($id, $fields = "*", $where = "", $order_by = "", $limit = "") {

    if ($where == "")
        $where = "where ";
    else
        $where = "$where and ";
    $sql = "select $fields from tema2 $where id_tema1=$id $order_by $limit";
    $sts = $GLOBALS['link']->query($sql);
    return getArray($sts);
}

function getTemaNivel2($id, $fields = "*") {
    $sql = "select $fields from tema2 where id_tema2=$id";
    $sts = $GLOBALS['link']->query($sql);
    return getObject($sts);
}

function getTemasNivel3($id, $fields = "*", $where = "", $order_by = "", $limit = "") {
    if ($where == "")
        $where = "where ";
    else
        $where = "$where and ";
    $sql = "select $fields from tema3 $where id_tema2=$id $order_by $limit";
    $sts = $GLOBALS['link']->query($sql);
    return getArray($sts);
}

function getTemaNivel3($id, $fields = "*") {
    $sql = "select $fields from tema3 where id_tema3=$id";
    $sts = $GLOBALS['link']->query($sql);
    return getObject($sts);
}

function updateEntidad($id, $desc) {
    $sql = "update entidad set desc_entidad='$desc' where clave_entidad=$id";
    $sts = $GLOBALS['link']->query($sql);
    return getEntidad($id);
}

function deleteEntidad($id) {
    $entidad = getEntidad($id);
    $sql = "delete from entidad where clave_entidad=$id";
    $sts = $GLOBALS['link']->query($sql);
    return $entidad;
}

function insertEntidad($desc) {
    $sql = "insert into entidad(desc_entidad)values('$desc')";
    $sts = $GLOBALS['link']->query($sql);
    $id = $GLOBALS['link']->insert_id;
    return getEntidad($id);
}

function updateMunicipio($id, $mun, $desc) {
    $sql = "update municipio set desc_municipio='$desc' where clave_municipio=$mun and clave_entidad=$id";
    $sts = $GLOBALS['link']->query($sql);
    return getMunicipio($id, $mun);
}

function deleteMunicipio($id, $mun) {
    $municipio = getMunicipio($id, $mun);
    $sql = "delete from municipio where clave_municipio=$mun and clave_entidad=$id";
    $sts = $GLOBALS['link']->query($sql);
    return $municipio;
}

function insertMunicipio($clave_entidad, $desc) {
    $sql = "insert into municipio(clave_entidad,desc_municipio)values($clave_entidad,'$desc')";
    $sts = $GLOBALS['link']->query($sql);
    $id = $GLOBALS['link']->insert_id;
    return getMunicipio($clave_entidad, $id);
}

function updateIndicador($id, $mun, $ind, $indicador) {
    $sql = "update indicador set indicador='$indicador' where clave_municipio=$mun and clave_entidad=$id and id_indicador=$ind";
    $sts = $GLOBALS['link']->query($sql);
    return getIndicador($id, $mun, $ind);
}

function insertIndicador($id, $mun, $id_tema_3, $indicador) {
    $query = "select max(id_indicador) as num from indicador ";
    $sts = mysqli_query($GLOBALS['link'], $query);
    $id_indicador = getObject($sts);
    $id_indicador = ($id_indicador->num + 1);
    $sql = "insert into indicador(id_indicador,indicador,id_tema3,clave_entidad,clave_municipio)" .
            " values($id_indicador,'$indicador',$id_tema_3,$id,$mun)";
    $sts = $GLOBALS['link']->query($sql);
    return getIndicador($id, $mun, $id_indicador);
}

function deleteIndicador($id, $mun, $ind) {
    $indicador = getIndicador($id, $mun, $ind);
    $sql = "delete from indicador where clave_municipio=$mun and clave_entidad=$id and id_indicador=$ind";
    $sts = $GLOBALS['link']->query($sql);
    return $indicador;
}

function updateValor($id, $mun, $ind, $anio, $valor) {
    $sql = "update valor set valor=$valor where id_indicador=$ind and anio=$anio";
    $sts = $GLOBALS['link']->query($sql);
    return getIndicador($id, $mun, $ind, $anio);
}

function insertValor($id, $mun, $ind, $anio, $valor, $unidad) {
    $sql = "insert into valor(anio,valor,unidad,id_indicador)values($anio,$valor,'$unidad',$ind)";
    $sts = $GLOBALS['link']->query($sql);
    return getIndicador($id, $mun, $ind, $anio);
}

function deleteValor($id, $mun, $ind, $anio) {
    $i = getIndicador($id, $mun, $ind, $anio);
    $sql = "delete from valor where id_indicador=$ind and anio=$anio";
    $sts = $GLOBALS['link']->query($sql);
    return $i;
}

function updateTemaNivel1($id, $nombre) {
    $sql = "update tema1 set nombre_tema1='$nombre' where id_tema1=$id ";
    $sts = $GLOBALS['link']->query($sql);
    return getTemaNivel1($id);
}

function insertTemaNivel1($nombre) {
    $sql = "insert into tema1(nombre_tema1)values('$nombre') ";
    $sts = $GLOBALS['link']->query($sql);
    $id = $GLOBALS['link']->insert_id;
    return getTemaNivel1($id);
}

function deleteTemaNivel1($id) {
    $res = getTemaNivel1($id);
    $sql = "delete from tema1 where id_tema1=$id ";
    $sts = $GLOBALS['link']->query($sql);
    return $res;
}

function updateTemaNivel2($id_tema1, $id, $nombre) {
    $sql = "update tema2 set nombre_tema2='$nombre' where id_tema2=$id and id_tema1=$id_tema1";
    $sts = $GLOBALS['link']->query($sql);
    return getTemaNivel2($id);
}

function insertTemaNivel2($id_tema1, $nombre) {
    $sql = "insert into tema2(id_tema1,nombre_tema2)values($id_tema1,'$nombre') ";
    $sts = $GLOBALS['link']->query($sql);
    $id = $GLOBALS['link']->insert_id;
    return getTemaNivel2($id);
}

function deleteTemaNivel2($id_tema1, $id) {
    $res = getTemaNivel2($id);
    $sql = "delete from tema2 where id_tema2=$id and id_tema1=$id_tema1";
    $sts = $GLOBALS['link']->query($sql);
    return $res;
}

function updateTemaNivel3($id_tema2, $id, $nombre) {
    $sql = "update tema3 set nombre_tema3='$nombre' where id_tema2=$id_tema2 and id_tema3=$id";
    $sts = $GLOBALS['link']->query($sql);
    return getTemaNivel3($id);
}

function insertTemaNivel3($id_tema1, $id_tema2, $nombre) {
    $sql = "insert into tema3(id_tema2,nombre_tema3)values($id_tema2,'$nombre') ";
    $sts = $GLOBALS['link']->query($sql);
    $id = $GLOBALS['link']->insert_id;
    return getTemaNivel3($id);
}

function deleteTemaNivel3($id_tema2, $id, $nombre) {
    $res = getTemaNivel3($id);
    $sql = "delete from tema3 where id_tema2=$id_tema2 and id_tema3=$id";
    $sts = $GLOBALS['link']->query($sql);
    return $res;
}

function updateIndicadorTema($id_tema3, $ind, $indicador) {
    $sql = "update indicador set indicador='$indicador' where id_tema3=$id_tema3 and id_indicador=$ind";
    $sts = $GLOBALS['link']->query($sql);
    return getInd($id_tema3, $ind);
}

function deleteIndicadorTema($id_tema3, $ind) {
    $res = getInd($id_tema3, $ind);
    $sql = "delete from indicador where id_tema3=$id_tema3 and id_indicador=$ind";
    $sts = $GLOBALS['link']->query($sql);
    return $res;
}

function updateValorTema($id_tema3, $ind, $anio, $valor) {
    $sql = "update valor set valor=$valor where id_indicador=$ind and anio=$anio";
    $sts = $GLOBALS['link']->query($sql);
    return getInd($id_tema3, $ind, $anio);
}

function deleteValorTema($id_tema3, $ind, $anio) {
    $res = getInd($id_tema3, $ind, $anio);
    $sql = "delete from valor where id_indicador=$ind and anio=$anio";
    $sts = $GLOBALS['link']->query($sql);
    return $res;
}

?>