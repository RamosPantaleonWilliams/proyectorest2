<?php

/**
 * $desc_entidad
 */
$app->put('/entidades/:id', function($id) use ($app_request, $app_response) {
            $desc_entidad = $app_request->put("desc_entidad");

            $xml = formatos($app_request);
            $data = array();
            $status = 200;
            if (!is_null($desc_entidad) && unique("entidad", "desc_entidad", $desc_entidad)) {
                try {
                    conect();
                    $data = updateEntidad($id, $desc_entidad);
                } catch (Exception $e) {
                    $status = 500;
                }
            } else {
                $status = 400;
            }
            respuesta($app_response, $status, "Entidad", $data, $xml);
        });



$app->put('/entidades/:id/municipios/:mun', function($id, $mun) use ($app_request, $app_response) {
            $desc_municipio = $app_request->put("desc_municipio");
            $xml = formatos($app_request);
            $data = array();
            $status = 200;
            if (!is_null($desc_municipio) && unique("municipio", "desc_municipio", $desc_municipio)) {
                try {
                    conect();
                    $data = updateMunicipio($id, $mun, $desc_municipio);
                } catch (Exception $e) {
                    $status = 500;
                }
            } else {
                $status = 400;
            }
            respuesta($app_response, $status, "Municipio", $data, $xml);
        });



$app->put('/entidades/:id/municipios/:mun/indicadores/:ind', function($id, $mun, $ind) use ($app_request, $app_response) {
            $indicador = $app_request->put("indicador");
            $xml = formatos($app_request);
            $data = array();
            $status = 200;
            if (!is_null($indicador) && unique("indicador", "indicador", $indicador)) {
                try {
                    conect();
                    $data = updateIndicador($id, $mun, $ind, $indicador);
                } catch (Exception $e) {
                    $status = 500;
                }
            } else {
                $status = 400;
            }
            respuesta($app_response, $status, "Indicador", $data, $xml);
        });



$app->put('/entidades/:id/municipios/:mun/indicadores/:ind/:anio', function($id, $mun, $ind, $anio) use ($app_request, $app_response) {
            $valor = $app_request->put("valor");
            $xml = formatos($app_request);
            $data = array();
            $status = 200;
            if (!is_null($valor) && unique("valor", "anio", $anio)) {
                try {
                    conect();
                    $data = updateValor($id, $mun, $ind, $anio, $valor);
                } catch (Exception $e) {
                    $status = 500;
                }
            } else {
                $status = 400;
            }
            respuesta($app_response, $status, "Indicador", $data, $xml);
        });






$app->put('/temas1/:id_tema1', function($id_tema1) use($app_request, $app_response) {
            $xml = formatos($app_request);
            $nombre_tema1 = $app_request->put("nombre_tema1");
            $datos = array();
            $status = 200;
            if (!is_null($nombre_tema1) && unique("tema1", "nombre_tema1", $nombre_tema1)) {
                try {
                    conect();
                    $datos = updateTemaNivel1($id_tema1, $nombre_tema1);
                } catch (Exception $e) {
                    $status = 500;
                }
            } else {
                $status = 400;
            }
            respuesta($app_response, $status, "Tema_Nivel_1", $datos, $xml);
        });


$app->put('/temas1/:id_tema1/temas2/:id_tema2', function($id_tema1, $id_tema2) use($app_request, $app_response) {
            $xml = formatos($app_request);
            $nombre_tema2 = $app_request->put("nombre_tema2");
            $datos = array();
            $status = 200;
            if (!is_null($nombre_tema2) && unique("tema2", "nombre_tema2", $nombre_tema2)) {
                try {
                    conect();
                    $datos = updateTemaNivel2($id_tema1, $id_tema2, $nombre_tema2);
                } catch (Exception $e) {
                    $status = 500;
                }
            } else {
                $status = 400;
            }
            respuesta($app_response, $status, "Tema_Nivel_2", $datos, $xml);
        });


$app->put('/temas1/:id_tema1/temas2/:id_tema2/temas3/:id_tema3', function($id_tema1, $id_tema2, $id_tema3) use($app_request, $app_response) {
            $xml = formatos($app_request);
            $nombre_tema3 = $app_request->put("nombre_tema3");
            $datos = array();
            $status = 200;
            if (!is_null($nombre_tema3) && unique("tema3", "nombre_tema3", $nombre_tema3)) {
                try {
                    conect();
                    $datos = updateTemaNivel3($id_tema2, $id_tema3, $nombre_tema3);
                } catch (Exception $e) {
                    $status = 500;
                }
            } else {
                $status = 400;
            }
            respuesta($app_response, $status, "Tema_Nivel_3", $datos, $xml);
        });


$app->put('/temas1/:id_tema1/temas2/:id_tema2/temas3/:id_tema3/indicadores/:ind', function($id_tema1, $id_tema2, $id_tema3, $ind) use($app_request, $app_response) {
            $xml = formatos($app_request);
            $indicador = $app_request->put("indicador");
            $datos = array();
            $status = 200;
            if (!is_null($indicador) && unique("indicador", "indicador", $indicador)) {
                try {
                    conect();
                    $datos = updateIndicadorTema($id_tema3, $ind, $indicador);
                } catch (Exception $e) {
                    $status = 500;
                }
            } else {
                $status = 400;
            }
            respuesta($app_response, $status, $meta, "Indicador", $xml);
        });

$app->put('/temas1/:id_tema1/temas2/:id_tema2/temas3/:id_tema3/indicadores/:ind/:anio', function($id_tema1, $id_tema2, $id_tema3, $ind, $anio) use($app_request, $app_response) {
            $xml = formatos($app_request);
            $anio = str_replace("." . $xml, "", $anio);
            $valor = $app_request->put("valor");
            $datos = array();
            $status = 200;
            if (!is_null($valor) && unique("valor", "anio", $anio)) {
                try {
                    conect();
                    $datos = updateValorTema($id_tema3, $ind, $anio, $valor);
                } catch (Exception $e) {
                    $status = 500;
                }
            } else {
                $status = 400;
            }
            respuesta($app_response, $status, "Valor", $datos, $xml);
        });
?>
